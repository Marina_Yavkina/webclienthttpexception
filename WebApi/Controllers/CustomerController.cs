using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Repository;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomerController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        [HttpGet("{id:long}")]   
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] long id)
        {

            var cst = await _unitOfWork.Customers.Get(id);
            if (cst == null) 
            {  
                return StatusCode(StatusCodes.Status404NotFound, cst); 
            }
           return StatusCode(StatusCodes.Status200OK, cst);
        }

        [HttpPost("")]   
        public async Task<ActionResult<Customer>> CreateCustomerAsync([FromBody] Customer customer)
        {
            if (ModelState.IsValid)
            {
                var cst = await _unitOfWork.Customers.Get(customer.Id);
                if (!(cst == null)) 
                {  return StatusCode(StatusCodes.Status409Conflict); }

                    
                // Customer ct = new Customer
                // {
                   
                //     Id =  (long)Math.Round( new Random().NextDouble() * 4294967 ),
                //     Firstname = customer.Firstname,
                //     Lastname = customer.Lastname

                // };
                 _unitOfWork.Customers.Create(customer);
                 await _unitOfWork.CompleteAsync();

                return StatusCode(StatusCodes.Status200OK, customer);
            }

            return BadRequest();
        }
    }
}
