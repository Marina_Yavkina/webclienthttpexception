using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class CustomerDto
    {
        public string Firstname { get; init; }

        public string Lastname { get; init; }
    }
}