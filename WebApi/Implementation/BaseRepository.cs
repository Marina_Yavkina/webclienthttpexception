using WebApi.Repository;
using WebApi.Models;
using WebApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace WebApi.Implemantations;

public class BaseRepository<T> : IBaseRepository<T> where T : class
{
    protected readonly DataContext Context;

    public BaseRepository(DataContext context)
    {
        Context = context;
    }
    
    public void Create(T entity)
    {
        Context.Add(entity);
    }

    // public virtual async Task<IEnumerable<T>> GetAll()
    // {
    //     return await _context.Set<T>().ToListAsync();
    // }
    public virtual  async Task<T> Get(long id)
    {

        return await Context.Set<T>().FindAsync(id);
    }

    public virtual async Task<List<T>> GetAll()
    {
         return await Context.Set<T>().ToListAsync();
    }
}
