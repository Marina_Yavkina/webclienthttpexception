using WebApi.Repository;
using WebApi.Models;
using WebApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using WebApi.Implemantations;

namespace WebApi.Implemantations;

public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
{
    public CustomerRepository(DataContext context) : base(context)
    {
    }
    
}