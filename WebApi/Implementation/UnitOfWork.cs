using WebApi.Repository;
using WebApi.Models;
using WebApi.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace WebApi.Implemantations{

public class UnitOfWork : IUnitOfWork
{
    private readonly DataContext _context;

    public UnitOfWork(DataContext context)
    {
        _context = context;
        Customers = new CustomerRepository(_context);  
    }

    public ICustomerRepository Customers { get; private set; }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }
    public void Dispose()
    {
        _context.Dispose();
    }
}
}