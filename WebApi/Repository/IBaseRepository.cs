using System.Threading.Tasks;
using WebApi.Models;
using System.Collections.Generic;

namespace WebApi.Repository;

public interface IBaseRepository<T> where T : class
{
    void Create(T entity);
  
    Task<T> Get(long id);
    Task<List<T>> GetAll();
}