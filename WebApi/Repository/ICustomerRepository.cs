using WebApi.Models;

namespace WebApi.Repository{

public interface ICustomerRepository : IBaseRepository<Customer>
{
}
}