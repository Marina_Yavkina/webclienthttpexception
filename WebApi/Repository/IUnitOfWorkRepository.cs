using System;
using System.Threading.Tasks;

namespace WebApi.Repository;

public interface IUnitOfWork: IDisposable
{
    ICustomerRepository Customers { get; }
    Task CompleteAsync();
}