﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;


namespace WebClient
{
    static class Program
    {
        static async Task Main(string[] args)
        {
           HttpClient client = new();
            Console.WriteLine("Введите id клиента");
            var client_id = Convert.ToString(Console.ReadLine());
            string url = "https://localhost:5001/customers/" + client_id;
            try
            {
                 var json = await client.GetStringAsync(url);
            Console.Write(json);
                  
            }
            catch(HttpRequestException httpRequestException) 
            {
                Console.WriteLine( httpRequestException.StatusCode);
                
            }
            var client1 = new HttpClient();
             CustomerCreateRequest cr = RandomCustomer();
            RandomGenerator rg = new RandomGenerator();


            Customer cl = new Customer{ Id = rg.randomLong(),Lastname = cr.Lastname, Firstname = cr.Firstname };
            
            string url_post = "https://localhost:5001/customers";
            
            var json1 = JsonSerializer.Serialize(cl);

            var data = new System.Net.Http.StringContent(json1, Encoding.UTF8, "application/json");
            //post customer
            try {

                var response = await client1.PostAsync(url_post, data);
                var response1 = await client1.PostAsync(url_post, data);
                response1.EnsureSuccessStatusCode();

            }
            catch(HttpRequestException httpRequestException) 
            {
                Console.WriteLine( httpRequestException.StatusCode);
                
            }
            string url_get = "https://localhost:5001/customers/" + cl.Id;
            var json2 = await client1.GetStringAsync(url_get);
            Console.Write(json2);
                      
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            RandomGenerator rg = new RandomGenerator();

            var fname = rg.randomString();
            var lname = rg.randomString();
            return new CustomerCreateRequest(fname,lname);
        }

       
    }
}