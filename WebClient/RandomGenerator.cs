using System;
namespace WebClient
{
    public class RandomGenerator
    {
        public long randomLong()
        {
            Random random = new Random();
            byte[] bytes = new byte[8];
            random.NextBytes(bytes);
            return BitConverter.ToInt64(bytes, 0);
        }

        public string randomString()
        {
            Random rand = new Random();
            int stringlen = rand.Next(4, 10);
            int randValue;
            string str = "";
            char letter;
            for (int i = 0; i < stringlen; i++)
            {
        
                // Generating a random number.
                randValue = rand.Next(0, 26);
        
                // Generating random character by converting
                // the random number into character.
                letter = Convert.ToChar(randValue + 65);
        
                // Appending the letter to string.
                str = str + letter;
                }
                return str;
            }
        }
}